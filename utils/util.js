const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}

const getRhythmRecords = _ => {
  return wx.getStorageSync('records') || []
}

const saveRhythmRecord = r => {
  let records = getRhythmRecords()
  records.splice(0, 0, r)
  wx.setStorage({
    key: 'records',
    data: records
  })
}

module.exports = {
  formatTime: formatTime,
  getRhythmRecords: getRhythmRecords,
  saveRhythmRecord: saveRhythmRecord
}
