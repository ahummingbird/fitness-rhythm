// voice.js

function playVoice(filePath) {
  const innerAudioContext = wx.createInnerAudioContext()
  innerAudioContext.autoplay = true
  innerAudioContext.src = filePath
  innerAudioContext.onPlay(() => {
    console.log('playing:' + filePath)
  })
  innerAudioContext.onError((res) => {
    console.log(res.errCode + ':' + res.errMsg)
  })
  return innerAudioContext
}

function counter(num) {
  playVoice('/media/number/' + num + '.mp3');
}

function repComplete(repCount) {
  playVoice('/media/number/' + repCount + '.mp3')
}

function setStart(setCount) {
  playVoice('/media/set' + setCount + '.mp3').onEnded(() => {
    //playVoice('/media/go.mp3')
  })
}

function setComplete() {
  playVoice('/media/set_completed.ogg')
}

function rhythmComplete() {
  playVoice('/media/done.mp3')
}

module.exports.counter = counter
exports.repComplete = repComplete
exports.setStart = setStart
exports.setComplete = setComplete
exports.rhythmComplete = rhythmComplete