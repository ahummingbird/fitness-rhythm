// quick_start.js
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    positions: app.globalData.positions,
    rhythm: {
      sets: 4,
      setDelay:45,
      reps: 12,
      repDelay: 2
    }
  },

  changePosition: function(evt) {
    let index = evt.detail.value
    let newRhythm = this.data.rhythm
    newRhythm.position = app.globalData.positions[index]
    this.setData({rhythm: newRhythm, invalidPosition: false})
  },

  changeSets: function(evt) {
    let newRhythm = this.data.rhythm;
    newRhythm.sets = evt.detail.value;
    this.setData({rhythm: newRhythm});
  },

  changeSetDelay: function (evt) {
    let newRhythm = this.data.rhythm;
    newRhythm.setDelay = evt.detail.value;
    this.setData({ rhythm: newRhythm });
  },

  changeReps: function (evt) {
    let newRhythm = this.data.rhythm;
    newRhythm.reps = evt.detail.value;
    this.setData({ rhythm: newRhythm });
  },

  changeRepDelay: function (evt) {
    let newRhythm = this.data.rhythm;
    newRhythm.repDelay = evt.detail.value;
    this.setData({ rhythm: newRhythm });
  },

  startRhythm: function(evt) {
    console.log(this.data.rhythm);
    if (!this.data.rhythm.position) {
      wx.showToast({
        title: '请选择锻炼部位!',
        icon: 'none',
        mast:true
      })
      this.setData({invalidPosition: true})
      return
    }
    wx.navigateTo({
      url: '../rhythm-player/rhythm-player?config=' + JSON.stringify(this.data.rhythm),
    })
  },

  cancel: function(evt) {
    wx.navigateBack({})
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  onShow: function() {
    console.log('on show rhythm = ' + JSON.stringify(this.data.rhythm))
  }
})