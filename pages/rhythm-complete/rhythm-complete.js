// pages/rhythm-complete/rhythm-complete.js
const app = getApp()
const util = require('../../utils/util.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    positions: app.globalData.positions
  },

  cont: function() {
    wx.navigateBack({})
  },

  changePosition: function (evt) {
    let index = evt.detail.value
    let newRhythm = this.data.rhythm
    newRhythm.position = app.globalData.positions[index]
    this.setData({ rhythm: newRhythm })
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log('receive options = ' + options)
    let rhythm = JSON.parse(options.rhythm)
    let userInfo = JSON.parse(options.user)
    let mode = options.mode || 'default'
    if (!rhythm.name) {
      rhythm.name = '未命名动作'
    }
    console.log(`receive rhythm: ${JSON.stringify(rhythm)}`)
    util.saveRhythmRecord(rhythm)
    this.setData({
      mode: mode,
      rhythm: rhythm,
      userInfo: userInfo
    });

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (options) {
    let userInfo = this.data.userInfo;
    let rhythm = this.data.rhythm;
    return {
      title: userInfo ? userInfo.nickName + '的训练记录' : '我的训练记录',
      path: '/pages/rhythm-complete/rhythm-complete?mode=poster&rhythm=' + JSON.stringify(rhythm) + '&user=' + JSON.stringify(userInfo),
      success: function (res) {
        // 转发成功
      },
      fail: function (res) {
        // 转发失败
      }
    }
  }
})