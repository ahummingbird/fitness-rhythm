// pages/rhythm-player/rhythm-player.js
const voice = require('../../utils/voice.js')
const app = getApp()
/**
 * 音效列表：
 *  - [x]5秒倒计时：开始锻炼前和组件休息结束
 *  - [x]数字：次数
 *  - []秒表
 *  - [x]组结束
 *  - [x]锻炼结束音效
 *  - [](*)背景音乐
 *  - [x](*)组报数: (第一组，第二组)
 */
Page({

  /**
   * 页面的初始数据
   */
  data: {
    active: false,
    workoutTime: 0,
    curSet: 0,
    curRep: 0,
    rhythm: null,
    countDown: null
  },

  start: function () {
    if (this.data.active) {
      console.log('already started, ignore current action.')
      return;
    }
    var self = this
    this.setData({ active: true, countDown: {title:'开始倒计时', seconds:5, callback: function(){
      voice.setStart(self.data.curSet + 1)
    }}});
    setTimeout(self.tick, 1000)
  },

  pause: function () {
    this.setData({ active: false });
  },

  cont: function() {
    this.setData({active: true});
    setTimeout(this.tick, 1000)
  },

  stop: function () {
    this.setData({ active: false, rhythem: null});
  },

  exit: function () {
    this.rhythmComplete();
  },

  /**
   * 单次动作完毕
   */
  repComplete: function(curRep) {
    let self = this;
    let rhythm = this.data.rhythm;
    voice.repComplete(curRep + 1)
    console.log(`rep:${curRep} complete.`)
  },

  /**
   * 单组训练完成
   */
  setComplete: function(curSet) {
    let self = this;
    let rhythm = this.data.rhythm;
    console.log(`set:${curSet} compmlete.`)
    voice.setComplete()
    self.setData({
      countDown: {title: '组间休息', seconds: rhythm.setDelay, callback: function () {
            voice.setStart(self.data.curSet + 1)
          }
        }
      })
  },

  /**
  * 全部训练完成
  */
  rhythmComplete: function() {
    voice.rhythmComplete();
    this.stop()
    this.data.rhythm.completeAt = new Date();
    let userInfo = app.globalData.userInfo;
    wx.redirectTo({
      url: '../rhythm-complete/rhythm-complete?rhythm=' + JSON.stringify(this.data.rhythm) + '&user=' + JSON.stringify(userInfo),
    })
  },

  tick: function() {
    let self = this;
    if(!self.countDownTick()) {
      let curSet = this.data.curSet;
      let curRep = this.data.curRep;
      let rhythm = this.data.rhythm;
      let workoutTime = self.data.workoutTime + 1

      let newSet = parseInt(workoutTime / (rhythm.reps * rhythm.repDelay))
      let newRep = parseInt((workoutTime % (rhythm.reps * rhythm.repDelay)) / rhythm.repDelay)
      this.setData({ workoutTime: workoutTime, curSet: newSet, curRep: newRep });

      console.log(`tick: ${workoutTime}, newSet: ${newSet}, newRep: ${newRep}`)

      if (newSet >= rhythm.sets) {
        this.rhythmComplete();
        return;
      }
      if (newRep != curRep) {
        this.repComplete(curRep)
      }
      if (newSet != curSet) {
        this.setComplete(curSet);
      }
    }
    if(self.data.active) {
      setTimeout(self.tick, 1000);
    }
  },

  /**
   * 倒计时，计时时间完成后调用callback
   * @param seconds - 持续时间，单位秒
   * @param callback - 计时完成后回调函数
   */

  countDownTick: function() {
    let self = this;
    let cd = self.data.countDown;
    if (!cd) {
      return false;
    }
    cd.seconds--;
    if (cd.seconds <= 5) {
      voice.counter(cd.seconds)
    }
    this.setData({countDown: cd})
    if (cd.seconds > 0) {
      return true;
    } else {
      console.log('count down tick finished.')
      this.setData({countDown: null})
      if(cd.callback) cd.callback()
      return false
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log('onLoad...')
    let rhythm = JSON.parse(options.config)
    if (!rhythm.name) {
      rhythm.name = '未命名动作'
    }
    rhythm.startAt = new Date()
    //let totalTime = rhythm.sets * rhythm.reps * rhythm.repDelay + rhythm.sets * rhythm.setDelay;
    console.log(`receive rhythm: ${JSON.stringify(rhythm)}`)
    this.setData({     
      rhythm: rhythm
    });
    wx.setKeepScreenOn({
      keepScreenOn: true
    })
  },

  onUnload: function() {
    console.log('onUnload...')
    this.stop();
  },

  onReady: function() {
    console.log('player - onReady.')
    this.start();
  },

  onShow: function () {
    console.log('player - onShow...')
    //this.start();
  },

  onHide: function() {
    console.log('player - onHide...')
    //this.pause();
  }
})